import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController, App, Loading, LoadingController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Store } from '@ngrx/store'
import { MainActions } from '../store/actions/mainActions';
import { AppStateI } from "../store/app-stats";
import { Observable } from 'rxjs/Rx';

import { AppConfig } from './app.config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'LoginPage';
  loader:Loading;
  public storeInfo:Observable<AppStateI>;
  @ViewChild('nav') nav: NavController;

  constructor(private config: AppConfig,
              private store: Store<any>,
              private mainActions: MainActions,
              private menuCtrl: MenuController,
              private app: App,
              platform: Platform, 
              statusBar: StatusBar, 
              private loadingCtrl:LoadingController,
              splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // Listen to user's authentication
      this.storeInfo = this.store.select((state:AppStateI) => state.currentUser)
      this.storeInfo.subscribe((currentState: any) => {
        if (currentState.currentUser) {
          this.app.getActiveNav().setRoot('ClientList');
        }
        else {
          this.app.getActiveNav().setRoot('LoginPage');
        }
      });

    });
  }

  // Show loader and dispatch check authentification
  ngOnInit() {
    this.presentLoading();
    this.store.dispatch(this.mainActions.checkAuth());
  }

  // Load an item in the menu
  onLoadMenu(page: string) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  // Log out
  onLogout() {
    this.store.dispatch(this.mainActions.logout());
    this.menuCtrl.close();
  }

  // Show the loader and dismiss on page changed
  presentLoading(){
    this.loader = this.loadingCtrl.create({
      content: "Authenticating...",
      dismissOnPageChange: true
    });
    this.loader.present();
  }

}
