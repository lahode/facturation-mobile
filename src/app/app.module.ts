// Default import
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// Ionic status bar and splash screen
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// App initializer
import { APP_INITIALIZER } from '@angular/core';

// Import HTTP Module
import { HttpModule, Http } from '@angular/http';

// Import Translation Module
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Import NGRX Tools
import { NgRxStoreModule } from "../store/store.module";

// Import App config
import { AppConfig } from './app.config';

import { ClientService } from '../services/client.service';


/**
 * Custom Http Loader for translation
 * (AoT requires an exported function for factories)
 */
export function HttpOBLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

/**
 * Custom Init Config function for loading environment configuration
 * (AoT requires an exported function for factories)
 */
export function initEnvConfig(config: AppConfig){
  return () => config.load() 
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    NgRxStoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpOBLoaderFactory,
        deps: [Http]
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
  ClientService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppConfig,
    { provide: APP_INITIALIZER,
      useFactory: initEnvConfig,
      deps: [AppConfig],
      multi: true
    },

  ]
})
export class AppModule {}
