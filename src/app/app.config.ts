import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AppConfig {
  private _config: Object = {};
  private _env: Object = null;
  private _path: string = '/assets/config/';

  constructor(private http: Http) {}

  /**
   * This method:
   *   a) Loads "env.json" to get the current working environment (e.g.: 'production', 'development')
   *   b) Loads "config.[env].json" to get all env's variables (e.g.: 'config.development.json')
   */
  public load(envName:string = "") {
    return new Promise((resolve, reject) => {
      if (!envName) {
        this.http.get(this._path + 'env.json').map( res => res.json() ).catch((error: any):any => {
          console.log('Configuration file "env.json" could not be read');
          resolve(true);
          return Observable.throw(error.json().error || 'Server error');
        }).subscribe( (envResponse) => {
          this._env = envResponse;
          let request:any = null;

          let tempEnv = 'development';
          switch (tempEnv) {
            case 'production':
            case 'development':
            case 'local':
            case 'desync': {
              request = this.http.get(this._path + 'config.' + tempEnv + '.json');
            } break;

            case 'default': {
              console.error('Environment file is not set or invalid');
              resolve(true);
            } break;
          }

          if (request) {
            request
              .map( res => res.json() )
              .catch((error: any) => {
                console.error('Error reading ' + tempEnv + ' configuration file');
                resolve(error);
                return Observable.throw(error.json().error || 'Server error');
              })
              .subscribe((responseData) => {
                this._config[tempEnv] = responseData;
                resolve(true);
              });
          } else {
            console.error('Env config file "env.json" is not valid');
            resolve(true);
          }
        });
      }
      else {
        this.http.get(this._path + 'config.' + envName + '.json')
          .map( res => res.json() )
          .catch((error: any) => {
            console.error('Error reading ' + envName + ' configuration file');
            resolve(error);
            return Observable.throw(error.json().error || 'Server error');
          })
          .subscribe((responseData) => {
            this._env['env'] = envName;
            this._config[envName] = responseData;
            resolve(true);
          });
      }
    });
  }

  /**
   * Use to get the data found in the first file (env file)
   */
  public getEnv(key: string = 'env') {
    return this._env[key];
  }

  /**
   * Use to set a new environment and load the datas
   */
  public setEnv(envName: string) {
    this.load(envName);
  }

  /**
   * Use to get config of the current environment
   */
  public get(key: string = 'env') {
    return this._config[this.getEnv()];
  }

}

/**
 * Export constants
 */
export class APP_CONST {

  public static countryList: any = {
    ch: 'Suisse',
    fr: 'France',
    de: 'Allemagne',
    au: 'Autriche',
    be: 'Belgique',
    dk: 'Danemark',
    es: 'Espagne',
    fi: 'Finlande',
    gb: 'Grande-Bretagne',
    il: 'Israël',
    it: 'Italie',
    li: 'Liechtenstein',
    lu: 'Luxembourg',
    no: 'Norvège',
    po: 'Portugal',
    se: 'Suède',
    tn: 'Tunisie',
  };

  public static civilStatusList: Array<string> = [
    '',
    'Célibataire',
    'Divorcé(e)',
    'Marié(e)',
    'Séparé(e)',
    'Veuf(ve)'
  ];

  public static genderList: Array<string> = [
    'M.',
    'Mme',
    'Mlle',
    'Dr.',
    'Prof.'
  ];
  
  public static insuranceList: Array<string> = [
    'Lamal',
    'Complémentaire'
  ];

  public static discountList: any = {
    0: 'Pas de rabais',
    0.1: '10%',
    0.2: '20%',
    0.3: '30%',
    0.5: '50%'
  };

};


