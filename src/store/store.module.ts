import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

// Import ngrx Tools
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Import ngRx Store
import { reducer } from './reducers';
import { AuthEffects } from './effects/auth-effects';
import { ClientEffects } from './effects/client-effects';
import { ErrorEffects } from './effects/error-effects';
import { MainActions } from './actions/mainActions';

// Import App services
import { AuthService } from '../services/auth.service';
import { AuthHttpService } from '../services/auth_http.service';
import { AlertService } from "../services/alert.service";
import { ClientService } from "../services/client.service";
import { LocalDataStorageService } from '../shared/localdata-storage.service';

const providers:Array<any> =  [
    AlertService,
    AuthService,
    AuthHttpService,
    ClientService,
    ClientEffects,
    LocalDataStorageService,
];

const effects:Array<any> = [
    AuthEffects,
    ClientEffects,
    ErrorEffects
];

const actions:Array<any> = [
    MainActions
];

@NgModule({
  imports: [
    HttpModule,
    EffectsModule.runAfterBootstrap(AuthEffects),
    EffectsModule.runAfterBootstrap(ClientEffects),
    EffectsModule.runAfterBootstrap(ErrorEffects),
    StoreModule.provideStore(reducer),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
  ],
  providers: [...providers, ...effects, ...actions]
})
export class NgRxStoreModule {};
