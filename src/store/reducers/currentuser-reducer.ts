import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface ICurrentUserState {
  currentUser?: any;
};

export const intitialState:ICurrentUserState = {};

export function reducer (state:ICurrentUserState = intitialState, action:Action):ICurrentUserState {
  switch (action.type) {
    case MainActions.LOGIN_SUCCESS:
      return Object.assign({}, state, { currentUser: action.payload })

    case MainActions.CHECK_AUTH_SUCCESS:
      return Object.assign({}, state, { currentUser: action.payload })

    case MainActions.LOGOUT_SUCCESS:
      return Object.assign({}, intitialState)

    default:
      return <ICurrentUserState>state;
  }
};
