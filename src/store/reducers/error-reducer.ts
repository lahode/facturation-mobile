import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface IErrorState {
  error?: any;
};

export const intitialState:IErrorState = {};

export function reducer (state:IErrorState = intitialState, action:Action):IErrorState {
  switch (action.type) {
    case MainActions.LOGIN_FAILED:
    case MainActions.CHECK_AUTH_FAILED:
      return Object.assign({}, state, { error: action.payload });

    default:
      return <IErrorState>state;
  }
};
