  import { Action } from "@ngrx/store";
  import { MainActions } from '../actions/mainActions';

  import { Client } from '../../models/client';

  export interface ICLientState {
    clientsFound: Client[],
    currentClient?: Client
  };

  export const initialState:ICLientState = {
    clientsFound : [],
  }

  export function reducer (state:ICLientState = initialState, action:Action):ICLientState {
    switch (action.type) {
      case MainActions.CLIENT_GETALL:
        return Object.assign({}, state);

      case MainActions.CLIENT_GETALL_SUCCESS:
        return Object.assign({}, state, { clientsFound: action.payload });

      case MainActions.CLIENT_FINDBY:
        return Object.assign({}, state);

      case MainActions.CLIENT_FINDBY_SUCCESS:
        return Object.assign({}, state, { clientsFound: action.payload });

      case MainActions.CLIENT_LOAD:
        return Object.assign({}, state);

      case MainActions.CLIENT_LOAD_SUCCESS:
        return Object.assign({}, state, { currentClient: action.payload });

      case MainActions.CLIENT_SAVE_SUCCESS:
        return Object.assign({}, state, {
          clientsFound: [...state.clientsFound.map((item: any) => {
            return item._id === action.payload.response._id ? action.payload.response : item;
          })]
        });

      case MainActions.CLIENT_DELETE_SUCCESS:
        return Object.assign({}, state, {
          clientsFound: [...state.clientsFound.filter((item: any) => {
              return item._id !== action.payload.queryParams.params._id;
          })]
        });

       case MainActions.LOGOUT_SUCCESS:
         return Object.assign({}, initialState);

       default:
         return <ICLientState>state;

    }
  };
