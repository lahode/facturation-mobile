import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface IAuthCheckedState {
  authChecked: boolean;
  currentCreds?: any
};

export const intitialState:IAuthCheckedState = {
  authChecked: false
};

export function reducer (state:IAuthCheckedState = intitialState, action:Action):IAuthCheckedState {
  switch (action.type) {
    case MainActions.LOGIN_SUCCESS:
      return Object.assign({}, state, { authChecked: true, currentCreds: action.payload.token })

    case MainActions.CHECK_AUTH_SUCCESS:
      return Object.assign({}, state, { authChecked: true })

    case MainActions.CHECK_AUTH_FAILED:
      return Object.assign({}, state, { authChecked: false })

    default:
      return <IAuthCheckedState>state;
  }
};
