import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface ILoadedState  {
  loaded: boolean;
};

export const intitialState:ILoadedState = {
  loaded: false
};

export function reducer (state:ILoadedState = intitialState, action:Action):ILoadedState {
  switch (action.type) {

    case MainActions.CHECK_AUTH_SUCCESS:
    case MainActions.LOGIN_SUCCESS:
    case MainActions.LOGOUT_SUCCESS:
    case MainActions.CLIENT_GETALL_SUCCESS:
    case MainActions.CLIENT_FINDBY_SUCCESS:
    case MainActions.CLIENT_LOAD_SUCCESS:
    case MainActions.CLIENT_SAVE_SUCCESS:
    case MainActions.CLIENT_DELETE_SUCCESS:
      return Object.assign({}, state, { loading: true });

    case MainActions.CHECK_AUTH:
    case MainActions.CHECK_AUTH_FAILED:
    case MainActions.LOGIN:
    case MainActions.LOGIN_FAILED:
    case MainActions.LOGOUT:
    case MainActions.CLIENT_GETALL:
    case MainActions.CLIENT_GETALL_FAILED:
    case MainActions.CLIENT_FINDBY:
    case MainActions.CLIENT_FINDBY_FAILED:
    case MainActions.CLIENT_LOAD:
    case MainActions.CLIENT_LOAD_FAILED:
    case MainActions.CLIENT_SAVE:
    case MainActions.CLIENT_SAVE_FAILED:
    case MainActions.CLIENT_DELETE:
    case MainActions.CLIENT_DELETE_FAILED:
      return Object.assign({}, state, { loading: false });

    default:
      return <ILoadedState>state;
  }
};
