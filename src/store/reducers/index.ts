import { combineReducers, ActionReducer, Action } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';
import { storeFreeze } from 'ngrx-store-freeze';

import * as fromLoading from './loading-reducer';
import * as fromLoaded from './loaded-reducer';
import * as fromQueryParams from './queryparams-reducer';
import * as fromCurrentUser from './currentuser-reducer';
import * as fromAuthCheck from './authchecked-reducer';
import * as fromError from './error-reducer';
import * as fromClient from './client-reducer';

import { AppStateI, RecucerStateI } from '../app-stats';

declare const process: any; // Typescript compiler will complain without this

const reducers:RecucerStateI = {
  loading: fromLoading.reducer,
  loaded: fromLoaded.reducer,
  queryParams: fromQueryParams.reducer,
  authCheck: fromAuthCheck.reducer,
  currentUser: fromCurrentUser.reducer,
  error: fromError.reducer,
  client: fromClient.reducer
};
const developmentReducer:ActionReducer<AppStateI> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<AppStateI> = combineReducers(reducers);

export function reducer(state: any, action: Action):AppStateI {
  let combineReducer:AppStateI = process.env.IONIC_ENV === 'prod' ? productionReducer(state, action) : developmentReducer(state, action);
  if(process.env.NODE_ENV === 'prod') { combineReducer = productionReducer(state, action) };
  return combineReducer
};
