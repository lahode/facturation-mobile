import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface IqueryParamsState {
  queryParams:{
    path:string
    params?:Object
  };
};

export const intitialState:IqueryParamsState = {
  queryParams: {path: '/'}
};

export function reducer (state:IqueryParamsState = intitialState, action:Action):IqueryParamsState {
  switch (action.type) {
    case MainActions.LOGOUT_SUCCESS:
      return Object.assign({}, intitialState)

    default:
      return <IqueryParamsState>state;
  }
};
