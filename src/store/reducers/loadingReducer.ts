import { Action } from "@ngrx/store";
import { MainActions } from '../actions/mainActions';

export interface ILoadingState {
  loading: boolean;
};

export const intitialState:ILoadingState = {
  loading: false
};

export function reducer (state:ILoadingState = intitialState, action:Action):ILoadingState {
  switch (action.type) {

    case MainActions.CHECK_AUTH:
    case MainActions.LOGIN:
    case MainActions.LOGOUT:
    case MainActions.CLIENT_GETALL:
    case MainActions.CLIENT_FINDBY:
    case MainActions.CLIENT_LOAD:
    case MainActions.CLIENT_SAVE:
    case MainActions.CLIENT_DELETE:
      return Object.assign({}, state, { loading: true });

    case MainActions.CHECK_AUTH_SUCCESS:
    case MainActions.CHECK_AUTH_FAILED:
    case MainActions.LOGIN_SUCCESS:
    case MainActions.LOGIN_FAILED:
    case MainActions.LOGOUT_SUCCESS:
    case MainActions.CLIENT_GETALL_SUCCESS:
    case MainActions.CLIENT_GETALL_FAILED:
    case MainActions.CLIENT_FINDBY_SUCCESS:
    case MainActions.CLIENT_FINDBY_FAILED:
    case MainActions.CLIENT_LOAD_SUCCESS:
    case MainActions.CLIENT_LOAD_FAILED:
    case MainActions.CLIENT_SAVE_SUCCESS:
    case MainActions.CLIENT_SAVE_FAILED:
    case MainActions.CLIENT_DELETE_SUCCESS:
    case MainActions.CLIENT_DELETE_FAILED:
     return Object.assign({}, state, { loading: false });

    default:
      return <ILoadingState>state;
  }
};
