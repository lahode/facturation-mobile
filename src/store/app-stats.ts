import { Action } from "@ngrx/store";

import { IqueryParamsState } from './reducers/queryparams-reducer';
import { ILoadingState } from './reducers/loading-reducer';
import { ILoadedState } from './reducers/loaded-reducer';
import { IAuthCheckedState } from './reducers/authchecked-reducer';
import { IErrorState } from './reducers/error-reducer';
import { ICurrentUserState } from './reducers/currentuser-reducer';
import { ICLientState } from './reducers/client-reducer';

export interface AppStateI {
  loading: ILoadingState,
  loaded: ILoadedState,
  authCheck: IAuthCheckedState,
  queryParams: IqueryParamsState,
  currentUser?: ICurrentUserState,
  error?: IErrorState,
  client: ICLientState
};


export interface RecucerStateI {
  loading: (state: ILoadingState, action: Action) => ILoadingState,
  loaded: (state: ILoadedState, action: Action) => ILoadedState,
  authCheck: (state: IAuthCheckedState, action: Action) => IAuthCheckedState,
  queryParams: (state: IqueryParamsState, action: Action) => IqueryParamsState,
  currentUser?: (state: ICurrentUserState, action: Action) => ICurrentUserState,
  error?: (state: IErrorState, action: Action) => IErrorState,
  client: (state: ICLientState, action: Action) => ICLientState,
};
