import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

import { User } from '../../models/user';
import { Client } from '../../models/client';

@Injectable()
export class MainActions {

  /* Login Action : AuthEffect */
  static LOGIN:string = 'LOGIN';
  static LOGIN_SUCCESS:string = 'LOGIN_SUCCESS';
  static LOGIN_FAILED:string = 'LOGIN_FAILED';
  login(user: User): Action {
    return <Action>{
      type: MainActions.LOGIN,
      payload: user
    }
  }

  /* Logout Action : AuthEffect */
  static LOGOUT:string = 'LOGOUT';
  static LOGOUT_SUCCESS:string = 'LOGOUT_SUCCESS';
  static LOGOUT_FAILED:string = 'LOGOUT_FAILED';
  logout(){
    return <Action>{
      type: MainActions.LOGOUT,
    }
  }

  /* Check Authentification Action : AuthEffect */
  static CHECK_AUTH:string = 'CHECK_AUTH';
  static CHECK_AUTH_SUCCESS:string = 'CHECK_AUTH_SUCCESS';
  static CHECK_AUTH_FAILED:string = 'CHECK_AUTH_FAILED';
  checkAuth():Action{
    return <Action>{
      type: MainActions.CHECK_AUTH,
    }
  }

  /* Get all clients Action : ClientEffect */
  static CLIENT_GETALL:string = 'CLIENT_GETALL';
  static CLIENT_GETALL_SUCCESS:string = 'CLIENT_GETALL_SUCCESS';
  static CLIENT_GETALL_FAILED:string = 'CLIENT_GETALL_FAILED';
  clientGetAll(userID: string):Action{
    return <Action>{
      type: MainActions.CLIENT_GETALL,
      payload: userID
    }
  }

  /* Find by client Action : ClientEffect */
  static CLIENT_FINDBY:string = 'CLIENT_FINDBY';
  static CLIENT_FINDBY_SUCCESS:string = 'CLIENT_FINDBY_SUCCESS';
  static CLIENT_FINDBY_FAILED:string = 'CLIENT_FINDBY_FAILED';
  clientFindBy(query: string):Action{
    return <Action>{
      type: MainActions.CLIENT_FINDBY,
      payload: query
    }
  }

  /* Load client Action : ClientEffect */
  static CLIENT_LOAD:string = 'CLIENT_LOAD';
  static CLIENT_LOAD_SUCCESS:string = 'CLIENT_LOAD_SUCCESS';
  static CLIENT_LOAD_FAILED:string = 'CLIENT_LOAD_FAILED';
  clientLoad(clientID: string):Action{
    return <Action>{
      type: MainActions.CLIENT_LOAD,
      payload: clientID
    }
  }

  /* Save client Action : ClientEffect */
  static CLIENT_SAVE:string = 'CLIENT_SAVE';
  static CLIENT_SAVE_SUCCESS:string = 'CLIENT_SAVE_SUCCESS';
  static CLIENT_SAVE_FAILED:string = 'CLIENT_SAVE_FAILED';
  clientSave(client: Client):Action{
    return <Action>{
      type: MainActions.CLIENT_SAVE,
      payload: client
    }
  }

  /* Delete client Action : ClientEffect */
  static CLIENT_DELETE:string = 'CLIENT_DELETE';
  static CLIENT_DELETE_SUCCESS:string = 'CLIENT_DELETE_SUCCESS';
  static CLIENT_DELETE_FAILED:string = 'CLIENT_DELETE_FAILED';
  clientDelete(clientID: string):Action{
    return <Action>{
      type: MainActions.CLIENT_DELETE,
      payload: clientID
    }
  }

}
