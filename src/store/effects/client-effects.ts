import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from "@ngrx/effects";

import { MainActions } from '../../store/actions/mainActions';
import { ClientService } from "../../services/client.service";

import { Client } from '../../models/client';

@Injectable()
export class ClientEffects {

  constructor(
    private action$: Actions,
    private _client: ClientService
  ) {}

  @Effect() getallAction$ = this.action$
      // Listen for the 'CLIENT_GETALL' action
      .ofType(MainActions.CLIENT_GETALL)
      .map<Action, any>(toPayload)
      .switchMap((payload:string) => {
        return this._client.getClients(payload)
      })
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CLIENT_FINDBY_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CLIENT_FINDBY_FAILED, payload: res })
      });

  @Effect() findbyAction$ = this.action$
      // Listen for the 'CLIENT_FINDBY' action
      .ofType(MainActions.CLIENT_FINDBY)
      .map<Action, any>(toPayload)
      .switchMap((payload:string) => {
        return this._client.getClientsLike(payload)
      })
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CLIENT_FINDBY_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CLIENT_FINDBY_FAILED, payload: res })
      });

  @Effect() loadAction$ = this.action$
      // Listen for the 'CLIENT_LOAD' action
      .ofType(MainActions.CLIENT_LOAD)
      .map<Action, any>(toPayload)
      .switchMap((payload:string) => {
        return this._client.getClient(payload)
      })
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CLIENT_LOAD_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CLIENT_LOAD_FAILED, payload: res })
      });

  @Effect() saveAction$ = this.action$
      // Listen for the 'CLIENT_SAVE' action
      .ofType(MainActions.CLIENT_SAVE)
      .map<Action, any>(toPayload)
      .switchMap((payload:Client) => {
        return this._client.save(payload)
      })
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CLIENT_SAVE_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CLIENT_SAVE_FAILED, payload: res })
      });

  @Effect() deleteAction$ = this.action$
      // Listen for the 'CLIENT_DELETE' action
      .ofType(MainActions.CLIENT_DELETE)
      .map<Action, any>(toPayload)
      .switchMap((payload:any) => {
        return this._client.delete(payload)
      })
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CLIENT_DELETE_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CLIENT_DELETE_FAILED, payload: res })
      });

}
