import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from "@ngrx/effects";

import { MainActions } from '../../store/actions/mainActions';
import { AlertService } from "../../services/alert.service";

@Injectable()
export class ErrorEffects {

  constructor(
    private action$: Actions,
    private _alert: AlertService
  ) {
  }

  @Effect() handleErrorAction$ = this.action$
      // Listen for the '*_FAILED' action
      .ofType(
        MainActions.LOGIN_FAILED,
      )
      .map<Action, any>(toPayload)
      .switchMap((payload:Observable<any>) => {
        return this._alert.doDisplayAlert(payload)
      })
}
