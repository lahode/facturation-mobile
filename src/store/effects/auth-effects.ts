import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { Effect, Actions, toPayload } from "@ngrx/effects";

import { MainActions } from '../../store/actions/mainActions';
import { AuthService } from "../../services/auth.service";

/*import { User } from '../../models/user';*/

@Injectable()
export class AuthEffects {

  constructor(
    private action$: Actions,
    private _auth: AuthService
  ) {
  }

  @Effect() loginAction$ = this.action$
      // Listen for the 'LOGIN' action
      .ofType(MainActions.LOGIN)
      .map<Action, any>(toPayload)
      .switchMap((payload) => this._auth.signin(payload))
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.LOGIN_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.LOGIN_FAILED, payload: res })
      })

  @Effect() checkAuthAction$ = this.action$
      // Listen for the 'CHECK_AUTH' action
      .ofType(MainActions.CHECK_AUTH)
      .switchMap<Action, any>(() => this._auth.isAuthenticated())
      .map<Action, any>((_result:any) => {
        return <Action>{ type: MainActions.CHECK_AUTH_SUCCESS, payload: _result }
      }).catch((res: any) => {
        return Observable.of({ type: MainActions.CHECK_AUTH_FAILED, payload: res })
      })

  @Effect() logoutAction$ = this.action$
      // Listen for the 'LOGOUT' action
      .ofType(MainActions.LOGOUT)
      .switchMap<Action, any>(() => this._auth.logout())
      // If successful, dispatch success action with result
      .flatMap<Action, any>(action =>{
        return Observable.concat(
          Observable.of(<Action>{ type: action.type }),
          Observable.of(<Action>{type: MainActions.LOGOUT_SUCCESS, payload: null })
        )
      })
      .catch((res: any) => Observable.of({ type: MainActions.LOGOUT_FAILED, payload: res }))

}
