import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';

// Store
import { Store, Action } from '@ngrx/store'
import { MainActions } from '../../store/actions/mainActions';

// Services and models
import { User } from "../../models/user";
import { AuthService } from "../../services/auth.service";

@IonicPage({
  name: 'LoginPage',
  segment: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: './login.html',
})
export class LoginPage{
  public loading: Loading;
  public loginForm:any;
  public user: User = new User('', '');

  constructor(private nav: NavController,
              private formBuilder: FormBuilder,
              private authService: AuthService, 
              private alertCtrl: AlertController,
              private store: Store<any>,
              private loadingCtrl: LoadingController,
              private mainActions: MainActions) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  /*
  onLogin() {
    this.showLoading();
    this.authService.signin(this.user)
      .subscribe(
        () => this.nav.setRoot('ClientPage'),
        err => {
          this.showError(err);
        }
      );
  }
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
  */

  onLogin() {
    if (this.loginForm.valid) {
      this.store.dispatch(<Action>this.mainActions.login(<User>this.loginForm.value));
    }
  }
 
  showError(text:string, hideLoading:boolean=true) : void {
    if (hideLoading === true){
      setTimeout(() => {
        this.loading.dismiss();
      });
    }
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
}