import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'ClientPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

}
