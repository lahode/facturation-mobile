import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ClientEdit } from './client-edit';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    ClientEdit
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(ClientEdit)
  ],
  exports: [
    ClientEdit
  ]
})
export class ClientModule {}
