import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup,  Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Rx';

// Store
import { Store, Action } from '@ngrx/store'
import { MainActions } from '../../../store/actions/mainActions';

import { User } from '../../../models/user';
import { Client } from '../../../models/client';

import { APP_CONST } from '../../../app/app.config';

@IonicPage()
@Component({
  selector: 'client-edit',
  templateUrl: 'client-edit.html'
})
export class ClientEdit {
  private clientIndex: string;
  client$: Observable<Client>;
  clientForm: FormGroup;
  civilStatusList: Array<string> = APP_CONST.civilStatusList;
  genderList: Array<string> = APP_CONST.genderList;
  insuranceList: Array<string> = APP_CONST.insuranceList;
  countryList: any = APP_CONST.countryList;;
  userList: User[] = [];
  datepickerOpts: any = {
    autoclose: true,
    todayBtn: 'linked',
    todayHighlight: true,
    assumeNearbyYear: true,
    format: 'dd.mm.yyyy'
  }

  // Charge les différentes classes et services
  constructor(private formBuilder: FormBuilder,
              private navCtrl: NavController,
              private navParams: NavParams,
              private store: Store<any>,
              private mainActions: MainActions) {
  }

  // Initialise le composant
  // Initialise le composant
  ionViewWillEnter() {
    // Initialiser le formulaire
    this.initForm();

    // Récupère l'ID du client
    this.clientIndex = this.navParams.get('clientID');
    if (this.clientIndex) {
      // Récupère les données du client
      this.client$ = this.store.select(state => state.client.currentClient);
      this.store.dispatch(<Action>this.mainActions.clientLoad(this.clientIndex));
    }
  }

  // Initialisation du formulaire de client
  private initForm() {
    let defaultClient : Client = new Client();
    let clientForm : any = {};

    Object.keys(defaultClient).forEach(key => {
      let value = defaultClient[key];
      clientForm[key] = [value];
    });
    //clientForm['user'] = [this.localStorage.getItem('userID')];
    clientForm['email'].push(Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"));
    clientForm['phone'].push(Validators.pattern("[\+][0-9]{9,15}"));
    clientForm['mobile'].push(Validators.pattern("[\+][0-9]{9,15}"));
    clientForm['prof_phone'].push(Validators.pattern("[\+][0-9]{9,15}"));
    clientForm['lastname'].push(Validators.required);
    clientForm['firstname'].push(Validators.required);
    clientForm['children'].push(Validators.pattern("[0-9]+"));
    clientForm['date'] = [new Date().toISOString(), Validators.required];

    this.clientForm = this.formBuilder.group(clientForm);
  }

  // Sauvegarde le client
  onSave() {
    let newClient = this.clientForm.value;
    if (this.clientIndex) {
      newClient._id = this.clientIndex;
    }
    this.store.dispatch(<Action>this.mainActions.clientSave(newClient));
  }

  // Annule les modification du client
  onCancel() {
    this.navCtrl.pop();
  }

}
