import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';

// Store
import { Store, Action } from '@ngrx/store'
import { MainActions } from '../../../store/actions/mainActions';

import { Client } from '../../../models/client';
import { APP_CONST } from '../../../app/app.config';

@IonicPage()
@Component({
  selector: 'tf-client-detail',
  templateUrl: './client-detail.html'
})
export class ClientDetail {
  client$: Observable<Client>;
  confirmDialogTitle: string = 'Suppression client';
  confirmDialogMessage : string = '';
  clientToDelete: string = '';
  clientDelete: boolean = false;
  countryList: any = APP_CONST.countryList;

  private clientIndex: string;

  // Charge les différentes classes et services
  constructor(private alertCtrl: AlertController,
              private navCtrl: NavController,
              private navParams: NavParams,
              private store: Store<any>,
              private mainActions: MainActions) {
  }

  // Initialise le composant
  ionViewWillEnter() {
    // Récupère l'ID du client
    this.clientIndex = this.navParams.get('clientID');
    if (this.clientIndex) {
      // Récupère les données du client
      this.client$ = this.store.select(state => state.client.currentClient);
      this.store.dispatch(<Action>this.mainActions.clientLoad(this.clientIndex));
    }
  }

  // Redirection vers l'édition du client
  onEdit() {
    this.navCtrl.push('ClientEdit', {clientID : this.clientIndex});
  }

  // Supprime le client
  onDelete(client: Client) {
    let confirm = this.alertCtrl.create({
      title: 'Supprimer le client',
      message: 'Êtes-vous sûr de vouloir supprimer le client ' + client.firstname + " " + client.lastname + " ?",
      buttons: [
        {
          text: 'Annuler',
        },
        {
          text: 'Confirmer',
          handler: () => {
            this.store.dispatch(<Action>this.mainActions.clientDelete(this.clientIndex));
          }
        }
      ]
    });
    confirm.present();
  }

}
