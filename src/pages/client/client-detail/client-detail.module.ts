import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ClientDetail } from './client-detail';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    ClientDetail,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(ClientDetail)
  ],
  exports: [
    ClientDetail,
  ]
})
export class ClientModule {}
