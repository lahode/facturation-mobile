import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'client-tabs',
  template: `
    <ion-tabs>
      <ion-tab [root]="ClientDetail" [rootParams]="clientID" tabTitle="Profile" tabIcon="person"></ion-tab>
    </ion-tabs>
  `
})
export class ClientTabs {
  clientID: string;

  // Charge les différentes classes et services
  constructor(public navParams: NavParams) {
    this.clientID = this.navParams.get('clientID');
  }
}
