import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ClientTabs } from './client-tabs';

@NgModule({
  declarations: [
    ClientTabs
  ],
  imports: [
    IonicPageModule.forChild(ClientTabs)
  ],
  exports: [
    ClientTabs
  ]
})
export class ClientModule {}
