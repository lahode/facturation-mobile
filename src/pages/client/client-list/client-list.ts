import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';

// Store
import { Store, Action } from '@ngrx/store'
import { MainActions } from '../../../store/actions/mainActions';

// Models
import { Client } from '../../../models/client';

@IonicPage()
@Component({
  selector: 'client-list',
  templateUrl: 'client-list.html'
})
export class ClientList {

  clientsQuery: string;
  clientsFound$ : Observable<Client[]>;

  constructor(public navCtrl: NavController,
              private store: Store<any>,
              private mainActions: MainActions) {
    this.clientsFound$ = this.store.select(state => state.client.clientsFound);
  }

  onKeyUp()  {
    if (this.clientsQuery.length) {
      this.store.dispatch(<Action>this.mainActions.clientFindBy(this.clientsQuery));
    }
  }

  // Redirection vers l'ajout d'un client
  onAdd() {
    this.navCtrl.push('ClientEdit');
  }

  // Redirection vers l'édition du client
  onView(clientID) {
    this.navCtrl.push('ClientDetail', {clientID : clientID});
  }

  // Redirection vers la visualisation du client
  onEdit(clientID) {
    this.navCtrl.push('ClientEdit', {clientID : clientID});
  }

}
