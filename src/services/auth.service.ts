import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/Rx'

import { User } from '../models/user';
import { AppConfig } from '../app/app.config';
import { LocalDataStorageService } from '../shared/localdata-storage.service';

const headers = { headers: new Headers({'Content-Type': 'application/json'}) };

@Injectable()
export class AuthService {

  private localTokenKey:string = 'tokenKey';
  private localUserId:string = 'userID';
  private localProfile:string = 'localProfile';
  private endpoint: string;

  private subject = new BehaviorSubject(null);
  
  public user$: Observable<User> = this.subject.asObservable();

  constructor(private http: Http,
              private config: AppConfig,
              private localStorage: LocalDataStorageService,
              private translateService: TranslateService) {
    this.endpoint = config.get().ENDPOINT;
  }

  // Save the token and user ID into the Local Storage
  private storeUserCredentials(token, userId) {
    this.localStorage.setItem(this.localTokenKey, token);
    this.localStorage.setItem(this.localUserId, userId);
  }

  // Remove the token and user ID into the Local Storage
  private removeUserCredentials() {
    this.localStorage.removeItem(this.localTokenKey);
    this.localStorage.removeItem(this.localUserId);
    this.localStorage.removeItem(this.localProfile);
  }

  // Check User token
  private checkUserCredentials() {
    return this.localStorage.getItem(this.localTokenKey) !== null;
  }

  // Signin the user
  public signin(user: User): Observable<any> {
    const body = JSON.stringify(user);
    return this.http.post(this.endpoint + '/authenticate', body, headers)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          this.storeUserCredentials(obj.data.token, obj.data.user._id);
          this.localStorage.setItem(this.localProfile, JSON.stringify(obj.data.user));
          if (!this.checkUserCredentials()) {
            throw (this.translateService.instant('LOGIN.ERRORS.TOKEN_REGISTER_FAILED'));
          }
          return obj.data.user;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('LOGIN.ERRORS.SIGNIN_FAILED'));
        }
      })
      .do(user => this.subject.next(user))
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Register a new user
  public register(user: User):Observable<any> {
    const body = JSON.stringify(user);
    return this.http.post(this.endpoint + '/register', body, headers)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          this.storeUserCredentials(obj.data.token, obj.data.user._id);
          this.localStorage.setItem(this.localProfile, JSON.stringify(obj.data.user));
          return obj.data.user;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('LOGIN.ERRORS.REGISTER_FAILED'));
        }
      })
      .do(user => this.subject.next(user))
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Request a new password
  public requestPassword(email: string):Observable<any> {
    return this.http.post(this.endpoint + '/password-request', JSON.stringify(email), headers)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.msg;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('LOGIN.ERRORS.PASSWORD_REQUEST_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Request a new e-mail confirmation
  public requestConfirmation(email: string):Observable<any> {
    return this.http.post(this.endpoint + '/confirmation-request', JSON.stringify(email), headers)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.msg;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('LOGIN.ERRORS.CONFIRMATION_REQUEST_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Logout the user (Wipe the Local storage)
  public logout() : Observable<any> {
    this.removeUserCredentials();
    return Observable.create((observer) => {
      observer.next(null)
    });
  };

  // Check if user is authenticated
  public isAuthenticated() : Observable<any> {
    return Observable.create((observer) => {
      if (!this.checkUserCredentials()) {
        throw (this.translateService.instant('LOGIN.ERRORS.AUTH_FAILED'));
      }
      observer.next(null)
    });
  }

}

