import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertService {

  constructor(private alertCtrl: AlertController) {}

  //Displaying State alert with Ionic AlertController
  doDisplayAlert(_payload):Observable<any>{
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: _payload,
      buttons: ['Dismiss']
    });
    alert.present();
    return Observable.create((observer) => {
      observer.next({ type: 'ERROR_DISPLAYED' })
    })
  }
}
