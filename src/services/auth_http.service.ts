import { Injectable } from "@angular/core";
import { Http, Headers, Response, Request, BaseRequestOptions, RequestMethod } from "@angular/http";
import { Observable } from "rxjs";
import { LocalDataStorageService } from '../shared/localdata-storage.service';

import { AppConfig } from '../app/app.config';

@Injectable()
export class AuthHttpService {

  private localTokenKey:string = 'tokenKey';


  constructor(private http: Http,
              private localStorage: LocalDataStorageService,
              private config: AppConfig) {}

  get(url:string):Observable<Response> {
    return this.request(url, RequestMethod.Get);
  }

  post(url:string, body:any):Observable<Response> {
    return this.request(url, RequestMethod.Post, body);
  }

  private request(url:string, method:RequestMethod, body?:any):Observable<Response>{

    let headers = new Headers();
    this.createAuthorizationHeader(headers);

    let options = new BaseRequestOptions();
    options.headers = headers;
    options.url = url;
    options.method = method;
    options.body = body;

    let request = new Request(options);

    return this.http.request(request);
  }

  // set the accept-language header using the value from i18n service that holds the language currently selected by the user
  private createAuthorizationHeader(headers:Headers) {
    headers.set('Authorization', this.localStorage.getItem(this.localTokenKey));
  }
}
