import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/Rx';

import { AuthHttpService } from './auth_http.service';
import { AppConfig } from '../app/app.config';
import { Client } from '../models/client';

@Injectable()
export class ClientService {

  private endpoint: string;

  constructor(private http: AuthHttpService,
              private config: AppConfig,
              private translateService: TranslateService) {
    this.endpoint = config.get().ENDPOINT;    
  }

  // Récupère 5 clients qui contiennent la chaine en entrée
  public getClientsLike(query: string):Observable<any> {
    return this.http.get(this.endpoint + '/app/client/getLike/' + query)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.data;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('CLIENT.ERRORS.GET_ALL_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(this.translateService.instant('CLIENT.ERRORS.GET_ALL_FAILED')));
  }

  // Récupère tous les clients
  public getClients(userID: string):Observable<any> {
    return this.http.get(this.endpoint + '/app/client/allByUser/' + userID)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.data;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('CLIENT.ERRORS.GET_ALL_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(this.translateService.instant('CLIENT.ERRORS.GET_ALL_FAILED')));
  }

  // Récupère le profil du client
  public getClient(clientID: string):Observable<any> {
    return this.http.get(this.endpoint + '/app/client/get/' + clientID)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          obj.data.birthdate = obj.data.birthdate ? new Date(obj.data.birthdate) : null;
          obj.data.date = obj.data.date ? new Date(obj.data.date) : null;
          return obj.data;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('CLIENT.ERRORS.GET_SINGLE_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Sauvegarde le client
  public save(client: Client):Observable<any> {
    return this.http.post(this.endpoint + '/app/client/save/', client)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.msg;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('CLIENT.ERRORS.SAVE_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

  // Supprime le client
  public delete(clientID: string):Observable<any> {
    return this.http.get(this.endpoint + '/app/client/delete/' + clientID)
      .map((response: Response) => {
        let obj = response.json();
        if (response.status == 200) {
          return obj.msg;
        }
        else {
          throw (obj.hasOwnProperty('msg') ? obj.msg : this.translateService.instant('CLIENT.ERRORS.DELETE_FAILED'));
        }
      })
      .publishLast().refCount()
      .catch((error: Response) => Observable.throw(error));
  }

}
