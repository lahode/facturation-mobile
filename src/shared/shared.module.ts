import { NgModule } from "@angular/core";

import { Nl2brPipe } from './nl2br.pipe';
import { ObjtoarrayPipe } from './objtoarray.pipe';

@NgModule({
  exports: [
    Nl2brPipe,
    ObjtoarrayPipe
  ],
  declarations: [
    Nl2brPipe,
    ObjtoarrayPipe
  ]
})
export class SharedModule {}
